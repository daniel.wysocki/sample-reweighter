class SampleReweighter(object):
    def __init__(self, prior_ref, prior_new):
        self.__prior_ref = prior_ref
        self.__prior_new = prior_new


    def __call__(self, samples):
        return self.__prior_new(samples) / self.__prior_ref(samples)
