# TODO: work out how to get equivalent of abc.ABC in Python 2.x

#import abc as _abc

#class Prior(_abc.ABC):
class Prior(object):
    def __init__(self):
        pass

#    @_abc.abstractmethod
    def __call__(self, samples):
        raise NotImplementedError()

#    @_abc.abstractproperty
    def ndim(self):
        raise NotImplementedError()



class UniformImproperPrior(Prior):
    def __init__(self, ndim):
        self.__ndim = ndim


    def __call__(self, samples):
        import numpy

        samples = numpy.asarray(samples)

        return numpy.ones(len(samples), dtype=numpy.float64)


    @property
    def ndim(self):
        return self.__ndim




class UniformBoxPrior(Prior):
    def __init__(self, bounds):
        self.bounds = bounds
        self.__ndim = len(bounds)


    def __call__(self, samples):
        import numpy

        samples = numpy.asarray(samples)
        out = numpy.ones(len(samples), dtype=bool)

        for param, (param_lo, param_hi) in zip(samples.T, self.bounds):
            out &= param >= param_lo
            out &= param <= param_hi

        return out.astype(numpy.float64)


    @property
    def ndim(self):
        return self.__ndim


class JointIndependentPrior(Prior):
    def __init__(self, priors):
        self.__priors = priors
        self.__ndim = sum(p.ndim for p in priors)


    def __call__(self, samples):
        import numpy

        samples = numpy.asarray(samples)
        out = numpy.ones(len(samples), dtype=numpy.float64)

        i = 0
        for prior in self.__priors:
            ndim = prior.ndim
            j = i+ndim

            subsamples = samples.T[i:j].T

            out *= prior(subsamples)

            i = j

        return out


    @property
    def ndim(self):
        return self.__ndim



class KDEPrior(Prior):
    def __init__(
            self,
            prior_samples, weights=None,
            reflections=None,
            bw_method="scott",
        ):
        from . import utils

        self.__ndim = len(prior_samples)
        self.__KDE = utils.bounded_kde(
            prior_samples.T, weights=weights,
            reflections=reflections,
            bw_method=bw_method,
        )


    def __call__(self, samples):
        return self.__KDE(samples.T)


    @property
    def ndim(self):
        return self.__ndim


class AlignedZPrior(Prior):
    def __init__(self, chi_max):
        self.__chi_max = chi_max

    def __call__(self, chi_z_samples):
        import numpy

        chi_z = chi_z_samples[...,0]

        return 0.5 * numpy.log(numpy.abs(self.__chi_max/chi_z)) / self.__chi_max

    @property
    def ndim(self):
        return 1
