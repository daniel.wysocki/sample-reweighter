# Sample Reweighter

```math
\langle f(X) \rangle_{\pi_{\mathrm{ref}}} =
\int f(x) \pi_{\mathrm{ref}}(x) p(d|x) \mathrm{d}x \approx
\frac{1}{N_{\mathrm{samples}}} \sum_{i=1}^{N_{\mathrm{samples}}} f(x_i),
```

where the $`x_i`$ are drawn from $`\pi_{\mathrm{ref}}(x) p(d|x)`$.

If one wants to change the prior to $`\pi_{\mathrm{new}}`$, without modifying the samples, the integral becomes

```math
\langle f(X) \rangle_{\pi_{\mathrm{new}}} =
\int f(x) \pi_{\mathrm{new}}(x) p(d|x) \mathrm{d}x =
\int f(x) \frac{\pi_{\mathrm{new}}(x)}{\pi_{\mathrm{ref}}(x)} \pi_{\mathrm{ref}}(x) p(d|x) \mathrm{d}x \approx
\frac{1}{N_{\mathrm{samples}}} \sum_{i=1}^{N_{\mathrm{samples}}} \frac{\pi_{\mathrm{new}}(x_i)}{\pi_{\mathrm{ref}}(x_i)} f(x_i),
```

where the distribution of $`x_i`$ have not changed. The Monte Carlo integral is just a weighted sum, with weights

```math
\mathrm{weight}(x_i) = \frac{\pi_{\mathrm{new}}(x_i)}{\pi_{\mathrm{ref}}(x_i)} \equiv w_i
```

This library provides the means to compute $`w_i`$.